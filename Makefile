.PHONY: init build up down shell lint

init:
	make down
	rm -rf ./backend/node_modules
	make build
	USER_ID=$$(id -u) GROUP_ID=$$(id -g) docker-compose run --no-deps --rm backend npm install

build:
	USER_ID=0 GROUP_ID=0 docker-compose build

up:
	USER_ID=$$(id -u) GROUP_ID=$$(id -g) docker-compose up --no-build ; \

down:
	USER_ID=$$(id -u) GROUP_ID=$$(id -g) docker-compose down --remove-orphans

shell:
	USER_ID=$$(id -u) GROUP_ID=$$(id -g) docker-compose exec backend /bin/sh || USER_ID=$$(id -u) GROUP_ID=$$(id -g) docker-compose run --rm --no-deps backend "/bin/sh"

lint:
	make down
	USER_ID=$$(id -u) GROUP_ID=$$(id -g) docker-compose run --no-deps --rm backend ./node_modules/.bin/eslint --max-warnings=0 'index.js'

