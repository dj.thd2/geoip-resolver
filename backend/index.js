const process = require('process');

const dotenv = require('dotenv');
dotenv.config();
if(process.env.DOTENV_SECONDARY_PATH) {
    dotenv.config({path: process.env.DOTENV_SECONDARY_PATH});
}

const fs = require('fs');
const crypto = require('crypto');
const Net = require('net');

const ip6addr = require('ip6addr');
const Parser = require('redis-parser');
const Maxmind = require('maxmind');
const BigIntBuffer = require('bigint-buffer');
const Errors = require('redis-errors');
const ParserError = Errors.ParserError;

const maxmindDatabaseBuffer = fs.readFileSync(process.env.MAXMIND_DATABASE_PATH);
const maxmindLookup = new Maxmind.Reader(maxmindDatabaseBuffer);

const port = 6379;

const dummyCallback = _ => _;

class LegacyParser {
    constructor(opts) {
        this.returnReply = opts?.returnReply || dummyCallback;
        this.returnError = opts?.returnError || dummyCallback;
        this.returnFatalError = opts?.returnFatalError || dummyCallback;
        this.pendingChunk = null;
    }

    reset() {
        this.pendingChunk = null;
    }

    execute(data) {
        const delim = data.indexOf('\r\n') >= 0 ? '\r\n' : '\n';
        let current = 0;
        while(true) {
            const next = data.indexOf(delim, current);
            let chunk;
            if(next == -1) {
                chunk = data.slice(current);
                if(chunk.length > 0) {
                    if(this.pendingChunk) {
                        this.pendingChunk = Buffer.concat([this.pendingChunk, chunk]);
                    } else {
                        this.pendingChunk = chunk;
                    }
                    console.log("pending chunk!", this.pendingChunk);
                }
                break;
            }
            if(this.pendingChunk) {
                chunk = Buffer.concat([this.pendingChunk, data.slice(current, next)]);
                this.pendingChunk = null;
            } else {
                chunk = data.slice(current, next);
            }
            //console.log("parse command line!");
            this.onParseCommandLine(chunk);
            current = next+delim.length;
        }
    }

    onParseCommandLine(data) {
        const args = data.toString().split(" ");
        let result = [];
        //console.log("args: ", args);
        for(let arg of args) {
            if(arg.length == 0) {
                continue;
            }
            result.push(arg);
        }
        if(result.length == 0) {
            // empty command, do nothing
            console.log("empty command");
            return;
        }
        //console.log("return reply", result);
        this.returnReply(result);
    }
}

const uppercaseAlphaRegex = /[A-Z]+/;
const newLineBuffer = Buffer.from("\r\n");
const dollarBuffer = Buffer.from("$");

class Controller {

    constructor(stream, opts) {
        this.isFinished = false;
        this.hasDataReceived = false;
        this.parser = null;
        this.stream = stream;
        this.onClose = opts?.onClose || dummyCallback;
    }

    useLegacyParser() {
       this.parser = new LegacyParser({
           returnReply: reply => this.onParserRequest(reply),
           returnError: err => this.onLegacyParserError(err),
           returnFatalError: err => this.onLegacyParserFatalError(err),
       });
    }

    useRespParser() {
        this.parser = new Parser({
            returnReply: reply => this.onParserRequest(reply),
            returnError: err => this.onParserError(err),
            returnFatalError: err => this.onParserFatalError(err),
            returnBuffers: false, // All strings are returned as Buffer e.g. <Buffer 48 65 6c 6c 6f>
            stringNumbers: false, // All numbers are returned as String
       });
    }

    processGeoipRequest(ip) {
       const parsedIp = ip6addr.parse(ip);
       const ipAddress = parsedIp.toString({format: 'auto'});

       const geoipResponse = maxmindLookup.get(ipAddress);

       const ipVersion = parseInt(parsedIp.kind().substr(3, 1));

       let ipNumber = null;

       if(ipVersion == 4) {
           ipNumber = parsedIp.toLong().toString()
       } else if(ipVersion == 6) {
           ipNumber = BigIntBuffer.toBigIntBE(parsedIp.toBuffer()).toString();
       }

       if(!geoipResponse) {
           return {
               result: "success",
               ipNumber: ipNumber,
               ipVersion: ipVersion,
               ipAddress: ipAddress,
               timezone: "unknown",
               countryName: "unknown",
               countryCode: "??",
               cityName: "unknown",
               regionName: "unknown",
           }
       }

       const geoipLocation = geoipResponse?.location;
       const geoipCity = geoipResponse?.city;
       const geoipCountry = geoipResponse?.country;
       const geoipSubdivisions = geoipResponse?.subdivisions;

       //console.log(geoipResponse);

       const geoipRegion = (geoipSubdivisions && geoipSubdivisions.length > 0) ? geoipSubdivisions[0] : null;
       const geoipSubregion = (geoipSubdivisions && geoipSubdivisions.length > 1) ? geoipSubdivisions[geoipSubdivisions.length-1] : null;

       return {
           result: "success",
           ipNumber: ipNumber,
           ipVersion: ipVersion,
           ipAddress: ipAddress,
           latitude: geoipLocation?.latitude,
           longitude: geoipLocation?.longitude,
           accuracy: geoipLocation?.accuracy_radius,
           timezone: geoipLocation?.time_zone,
           countryName: geoipCountry?.names?.en,
           countryCode: geoipCountry?.iso_code,
           countryGeonameId: geoipCountry?.geoname_id,
           isEuropeanUnion: geoipCountry?.is_in_european_union,
           zipCode: geoipResponse?.postal?.code,
           cityName: geoipCity?.names?.en,
           cityGeonameId: geoipCity?.geoname_id,
           regionName: geoipRegion?.names?.en,
           regionCode: geoipRegion?.iso_code,
           regionGeonameId: geoipRegion?.geoname_id,
           subregionName: geoipSubregion?.names?.en,
           subregionCode: geoipSubregion?.iso_code,
           subregionGeonameId: geoipSubregion?.geoname_id,
       }
    }

    onStreamData(data) {
        if(this.isFinished) {
            return;
        }
        if(!this.hasDataReceived) {
            this.hasDataReceived = true;
            const firstChar = data.slice(0, 1).toString();
            if(firstChar == "*") {
                this.useRespParser();
            } else if(!uppercaseAlphaRegex.test(firstChar.toUpperCase())) {
                this.finishErrorResponse("Invalid command");
                return;
            } else {
                this.useLegacyParser();
            }
        }
        if(!this.parser) {
            this.finishErrorResponse("Invalid command");
            return;
        }
        this.parser.execute(data);
    }

    onParserRequest(data) {
        //console.log("parsed request", data);
        if(!data || data.length == 0) {
            this.finishErrorResponse("Invalid command");
            return;
        }
        try {
            const command = data[0].toUpperCase();
            if(command.length > 16 || !uppercaseAlphaRegex.test(command)) {
                this.finishErrorResponse("Invalid command");
                return;
            }
            switch(data[0].toUpperCase()) {
                case "GET":
                    if(!data[1] || data[1].length == 0) {
                        this.sendErrorResponse("Error: " + String(err));
                        break;
                    }
                    let result;
                    switch(data[1]) {
                        case "info":
                            result = {
                                vendor: 'maxmind',
                                edition: process.env?.MAXMIND_EDITION,
                                etag: process.env?.CURRENT_ETAG,
                                filesize: process.env?.CURRENT_SIZE,
                                filename: process.env?.CURRENT_FILENAME,
                                last_modified: process.env?.CURRENT_UPDATE_DATE,
                            };
                            break;
                        default:
                            result = this.processGeoipRequest(data[1]);
                            break;
                    }
                    this.sendStringResponse(JSON.stringify(result));
                    break;
                case "PING":
                    if(data[1]) {
                        this.sendStringResponse(data[1]);
                    } else {
                        this.sendSimpleStringResponse("PONG");
                    }
                    break;
                default:
                    this.sendOkResponse();
                    break;
            }
        } catch(err) {
            console.error(err);
            this.sendErrorResponse("Error: " + String(err));
        }
    }

    onStreamFinishing() {
        this.isFinished = true;
        if(this.parser) {
            this.parser.reset();
            this.parser = null;
        }
    }

    onStreamFinished() {
        if(this.stream) {
            this.stream.resetAndDestroy();
            this.stream = null;
        }
        this.onClose();
    }

    onStreamError(err) {
        console.error("stream error", err);
        this.onStreamFinishing();
        this.onStreamFinished();
    }

    onStreamEnd() {
        if(!this.isFinished) {
            this.onStreamFinishing();
            this.onStreamFinished();
        }
    }

    onParserError(err) {
        console.error("parser error", err);
        this.sendErrorResponse("Invalid command");
    }

    onParserFatalError(err) {
        console.error("parser fatalError", err);
        this.finishErrorResponse("Invalid command: ", JSON.stringify(err));
    }

    onLegacyParserError(err) {
        console.error("legacy parser error", err);
        this.sendErrorResponse("Invalid command");
    }

    onLegacyParserFatalError(err) {
        console.error("legacy parser fatal error", err);
        this.sendErrorResponse("Invalid command: ", JSON.stringify(err));
    }

    sendStringResponse(data) {
        const dataBuffer = Buffer.from(data, "utf-8");
        this.stream.write(Buffer.concat([dollarBuffer, Buffer.from(dataBuffer.length.toString()), newLineBuffer, dataBuffer, newLineBuffer]));
    }

    sendOkResponse() {
        this.stream.write("+OK\r\n");
    }

    sendSimpleStringResponse(data) {
        this.stream.write("+" + data + "\r\n");
    }

    sendErrorResponse(data) {
        this.stream.write("-" + data + "\r\n");
    }

    finishErrorResponse(data) {
        this.onStreamFinishing();
        this.sendErrorResponse(data);
        this.onStreamFinished();
    }

    close() {
        this.finishErrorResponse("Server is shutting down");
    }
}

const server = new Net.Server();

let isServerClosing = false;
let clients = {};

const cleanupClients = function() {
    isServerClosing = true;
    for(let clientEntry of Object.entries(clients)) {
        clientEntry[1].close();
    }
}

server.on('connection', function(socket) {
    if(isServerClosing) {
        socket.write("-Server is shutting down\r\n");
        socket.resetAndDestroy();
    } else {
        const clientId = crypto.randomBytes(8).toString('hex');
        const controller =  new Controller(socket, {
            onClose: () => { delete clients[clientId]; }
        });
        clients[clientId] = controller;
        socket.on('data', data => controller.onStreamData(data));
        socket.on('error', err => controller.onStreamError(err));
        socket.on('end', _ => controller.onStreamEnd());
        socket.on('close', _ => controller.onStreamEnd());
        socket.on('destroy', _ => controller.onStreamEnd());
    }
});

server.on('close', function() {
    cleanupClients();
});

const onProcessExit = () => {
    console.log('onProcessExit, closing server');
    server.close(() => {
        console.log('Server closed');
    });
    cleanupClients();
}

process.on('SIGTERM', () => {
    console.log('Received sigterm');
    onProcessExit();
});

process.on('SIGINT', () => {
    console.log('Received sigterm');
    onProcessExit();
});

process.on('SIGHUP', () => {
    console.log('Received sighup');
    onProcessExit();
});

process.on('SIGQUIT', () => {
    console.log('Received sigquit');
    onProcessExit();
});

server.listen(port, '0.0.0.0', function() {
    console.log(`Server listening for connection requests on port ${port}`);
});
